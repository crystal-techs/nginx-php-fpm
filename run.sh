#!/bin/sh

cp -Ru /default/* /DATA
chown -Rf nginx:nginx /DATA/public_html

# start nginx
mkdir -p /tmp/nginx
chown nginx:nginx /tmp/nginx
chown -Rf nginx:nginx /DATA/logs/
rm -f /etc/php7/php.ini
ln -s /DATA/config/php.ini /etc/php7/php.ini
rm -f /etc/php7/php-fpm.conf
ln -s /DATA/config/php-fpm.conf /etc/php7/php-fpm.conf
rm -f /etc/nginx/nginx.conf
ln -s /DATA/config/nginx.conf /etc/nginx/nginx.conf

php-fpm7
nginx