FROM alpine:latest
MAINTAINER Nguyen Cap Tien. <nguyen@crystal-techs.com>

RUN apk update \
    && apk add --no-cache nginx nginx-mod-http-echo ca-certificates \
    php7-fpm php7-json php7-zlib php7-xml php7-pdo php7-phar php7-openssl \
    php7-pdo_mysql php7-mysqli php7-session \
    php7-gd php7-iconv php7-mcrypt \
    php7-curl php7-opcache php7-ctype php7-apcu \
    php7-intl php7-bcmath php7-mbstring php7-dom php7-xmlreader mysql-client curl && apk add -u --no-cache musl && apk --update --no-cache add tar zip

RUN rm -rf /var/cache/apk/*

RUN sed -i 's/;cgi.fix_pathinfo=1/cgi.fix_pathinfo=0/g' /etc/php7/php.ini && \
    sed -i "s/nginx:x:100:101:nginx:\/var\/lib\/nginx:\/sbin\/nologin/nginx:x:100:101:nginx:\/DATA:\/bin\/bash/g" /etc/passwd && \
    sed -i "s/nginx:x:100:101:nginx:\/var\/lib\/nginx:\/sbin\/nologin/nginx:x:100:101:nginx:\/DATA:\/bin\/bash/g" /etc/passwd- && \
    ln -s /usr/bin/php7 /usr/bin/php && \
    ln -s /sbin/php-fpm7 /sbin/php-fpm

ADD default/ /default
ADD run.sh /
RUN chmod +x /run.sh

EXPOSE 80
VOLUME ["/DATA"]

CMD ["/run.sh"]